<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Throwable $exception
     * @return \Illuminate\Http\Response
     * @throws Throwable
     */
    public function render($request, Throwable $e)
    {
        if ($e instanceof NotFoundHttpException) {
            return response(['status' => 404, 'message' => 'The requested resource was not found']);
        }

        if ($e->getCode()) {
            return response(['status' => 500, 'message' => 'Internal Server Error']);
        }

        return parent::render($request, $e);
    }

    /**
     * Render the given HttpException.
     *
     * @param HttpExceptionInterface $e
     * @return Response
     */
    protected function renderHttpException(HttpExceptionInterface $e)
    {
        $status = $e->getCode() != 0 ? $e->getCode() : 500;

        return response(['status' => $status, 'message' => $e->getMessage()]);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json((['status' => 401, 'message' => 'Unauthorized']), 401);
    }
}
