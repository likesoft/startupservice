<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('api_token', 80)->after('password')
                ->unique()
                ->nullable()
                ->default(null);
            $table->string('fio')->nullable();
            $table->string('city')->nullable();
            $table->string('occupation')->nullable();
            $table->boolean('combinedwork')->nullable();
            $table->boolean('startup_main_source_of_income')->nullable();
            $table->boolean('had_negative_experience')->nullable();
            $table->string('phone', 12)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['fio', 'city', 'phone', 'api_token']);
        });
    }
}
